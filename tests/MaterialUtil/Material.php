<?php

namespace MaterialUtil;

use Config;
use stlswm\WxSubscriptionPHP\Material\MaterialUtil;
use stlswm\WxSubscriptionPHP\Response;

require __DIR__.'/../../vendor/autoload.php';
require __DIR__.'/../Config.php';

class Material extends \PHPUnit\Framework\TestCase
{
    public function testGetMaterialCount()
    {
        $res = MaterialUtil::getMaterialCount(Config::$accessToken);
        var_dump($res);
        $this->assertInstanceOf(Response::class, $res);
    }

    public function testBatchGetMaterial()
    {
        $res = MaterialUtil::batchGetMaterial(Config::$accessToken);
        print_r($res->data);
        $this->assertInstanceOf(Response::class, $res);
    }
}