<?php

namespace Message;

use PHPUnit\Framework\TestCase;

require __DIR__.'/../../vendor/autoload.php';
require __DIR__.'/../Config.php';

class MessageFactory extends TestCase
{
    public function testCreateImageMsg()
    {
        $dom = \stlswm\WxSubscriptionPHP\Message\MessageFactory::createImageMsg('wm', 'hpw', 'media id of image');
        echo $dom;
        $this->assertIsString($dom);
    }
}