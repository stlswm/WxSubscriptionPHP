<?php

namespace stlswm\WxSubscriptionPHP\User;

use stlswm\WxSubscriptionPHP\CurlHttp;
use stlswm\WxSubscriptionPHP\Response;

/**
 * Class UserInfo
 *
 * @package WxSubscriptionPHP\User
 * @Date    2018/12/29
 * @Time    15:13
 */
class UserInfo
{
    use CurlHttp;

    /**
     * 获取用户信息
     *
     * @param        $accessToken
     * @param        $openId
     * @param string $lang
     *
     * @return Response
     * @Author wm
     * @Date   2018/8/16
     * @Time   15:31
     */
    public static function getInfo(string $accessToken, string $openId, $lang = 'zh_CN'): Response
    {
        if (empty($accessToken)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数$accessToken不能为空';
            return $response;
        }
        if (empty($openId)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数$openId不能为空';
            return $response;
        }
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={$accessToken}&openid={$openId}&lang={$lang}";
        return self::get($url);
    }
}
