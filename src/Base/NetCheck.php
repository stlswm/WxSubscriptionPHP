<?php

namespace stlswm\WxSubscriptionPHP\Base;

use stlswm\WxSubscriptionPHP\CurlHttp;
use stlswm\WxSubscriptionPHP\Response;

/**
 * Class NetCheck
 * 网络检测
 * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=21541575776DtsuT
 *
 * @package WxSubscriptionPHP\Base
 * @Date    2018/12/29
 * @Time    10:22
 */
class NetCheck
{
    use CurlHttp;

    /**
     * @var string 接口地址
     */
    private static $api = 'https://api.weixin.qq.com/cgi-bin/callback/check';

    /**
     * @param string $accessToken
     *
     * @return Response
     * @Author wm
     * @Date   2018/12/29
     * @Time   13:46
     */
    public static function check(string $accessToken): Response
    {
        if (empty($accessToken)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数access_token不能为空';
            return $response;
        }
        $address = self::$api . '?access_token=' . $accessToken;
        return self::get($address);
    }
}