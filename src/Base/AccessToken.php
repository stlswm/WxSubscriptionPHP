<?php

namespace stlswm\WxSubscriptionPHP\Base;

use stlswm\WxSubscriptionPHP\CurlHttp;
use stlswm\WxSubscriptionPHP\Response;

/**
 * Class AccessToken
 * 更新token
 * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140183
 *
 * @package WxSubscriptionPHP\Base
 * @Date    2018/12/29
 * @Time    13:44
 */
class AccessToken
{
    use CurlHttp;
    private static $api = 'https://api.weixin.qq.com/cgi-bin/token';

    /**
     * 更新token
     *
     * @param string $appId
     * @param string $appSecret
     *
     * @return Response
     * @Author wm
     * @Date   2018/12/29
     * @Time   13:46
     */
    public static function refresh(string $appId, string $appSecret): Response
    {
        if (empty($appId)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数app_id不能为空';
            return $response;
        }
        if (empty($appSecret)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数app_secret不能为空';
            return $response;
        }
        $address = self::$api;
        $address .= "?grant_type=client_credential&appid={$appId}&secret={$appSecret}";
        return self::get($address);
    }
}