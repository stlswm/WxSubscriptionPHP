<?php

namespace stlswm\WxSubscriptionPHP\Message;

use DOMDocument;
use DOMException;

/**
 * Class WeChatMessage
 * @package lib\we_chat\subscription
 * @Date    2018/8/15
 * @Time    14:19
 */
class MessageFactory
{

    /**
     * 文本消息
     * @param  string  $toUser
     * @param  string  $fromUser
     * @param  string  $content
     * @return string
     * @Author: wm
     * @Date  : 19-2-15
     * @Time  : 上午11:23
     * @throws DOMException
     */
    public static function createTextMsg(string $toUser, string $fromUser, string $content): string
    {
        $dom = new DOMDocument('1.0', 'utf-8');
        $xml = $dom->createElement('xml');
        $to_user_mame = $dom->createElement("ToUserName");
        $to_user_mame->appendChild($dom->createCDATASection($toUser));
        $from_user_name = $dom->createElement("FromUserName");
        $from_user_name->appendChild($dom->createCDATASection($fromUser));
        $create_time = $dom->createElement("CreateTime");
        $create_time->appendChild($dom->createCDATASection(time()));
        $msg_type = $dom->createElement("MsgType");
        $msg_type->appendChild($dom->createCDATASection('text'));
        $content_dom = $dom->createElement("Content");
        $content_dom->appendChild($dom->createCDATASection($content));
        $xml->appendChild($to_user_mame);
        $xml->appendChild($from_user_name);
        $xml->appendChild($create_time);
        $xml->appendChild($msg_type);
        $xml->appendChild($content_dom);
        $dom->appendChild($xml);
        return $dom->saveXML();
    }

    /**
     * 创建图片消息
     * @param  string  $toUser
     * @param  string  $fromUser
     * @param  string  $mediaId
     * @return string
     * @throws DOMException
     */
    public static function createImageMsg(string $toUser, string $fromUser, string $mediaId): string
    {
        $dom = new DOMDocument('1.0', 'utf-8');
        $xml = $dom->createElement('xml');
        $to_user_mame = $dom->createElement("ToUserName");
        $to_user_mame->appendChild($dom->createCDATASection($toUser));
        $from_user_name = $dom->createElement("FromUserName");
        $from_user_name->appendChild($dom->createCDATASection($fromUser));
        $create_time = $dom->createElement("CreateTime");
        $create_time->appendChild($dom->createCDATASection(time()));
        $msg_type = $dom->createElement("MsgType");
        $msg_type->appendChild($dom->createCDATASection('image'));
        $media_id = $dom->createElement('MediaId');
        $media_id->appendChild($dom->createCDATASection($mediaId));
        $content_dom = $dom->createElement("Image");
        $content_dom->appendChild($media_id);
        $xml->appendChild($to_user_mame);
        $xml->appendChild($from_user_name);
        $xml->appendChild($create_time);
        $xml->appendChild($msg_type);
        $xml->appendChild($content_dom);
        $dom->appendChild($xml);
        return $dom->saveXML();
    }

    /**
     * 图文消息
     * @param  string  $toUser
     * @param  string  $fromUser
     * @param  array   $itemMap
     * @return string
     * @Author wm
     * @Date   2018/8/15
     * @Time   14:19
     * @throws DOMException
     */
    public static function createImageTextMsg(string $toUser, string $fromUser, array $itemMap): string
    {
        $dom = new DOMDocument('1.0', 'utf-8');
        $xml = $dom->createElement('xml');
        $to_user_mame = $dom->createElement("ToUserName");
        $to_user_mame->appendChild($dom->createCDATASection($toUser));
        $xml->appendChild($to_user_mame);
        $from_user_name = $dom->createElement("FromUserName");
        $from_user_name->appendChild($dom->createCDATASection($fromUser));
        $xml->appendChild($from_user_name);
        $create_time = $dom->createElement("CreateTime");
        $create_time->appendChild($dom->createCDATASection(time()));
        $xml->appendChild($create_time);
        $msg_type = $dom->createElement("MsgType");
        $msg_type->appendChild($dom->createCDATASection('news'));
        $xml->appendChild($msg_type);
        $article_count = $dom->createElement("ArticleCount");
        $article_count->nodeValue = count($itemMap);
        $xml->appendChild($article_count);
        $articles = $dom->createElement('Articles');
        foreach ($itemMap as $val) {
            $item = $dom->createElement('item');
            $title = $dom->createElement('Title');
            $description = $dom->createElement('Description');
            $pic_url = $dom->createElement('PicUrl');
            $url = $dom->createElement('Url');
            $title->appendChild($dom->createCDATASection($val['title']));
            $description->appendChild($dom->createCDATASection($val['description']));
            $pic_url->appendChild($dom->createCDATASection($val['picUrl']));
            $url->appendChild($dom->createCDATASection($val['url']));
            $item->appendChild($title);
            $item->appendChild($description);
            $item->appendChild($pic_url);
            $item->appendChild($url);
            $articles->appendChild($item);
            unset($item);
            unset($title);
            unset($description);
            unset($pic_url);
            unset($url);
        }
        $xml->appendChild($articles);
        $dom->appendChild($xml);
        return $dom->saveXML();
    }
}