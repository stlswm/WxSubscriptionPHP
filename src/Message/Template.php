<?php

namespace stlswm\WxSubscriptionPHP\Message;

use stlswm\WxSubscriptionPHP\CurlHttp;
use stlswm\WxSubscriptionPHP\Response;

/**
 * Class Template
 *
 * @package WxSubscriptionPHP\Message
 * @Date    2018/12/29
 * @Time    14:42
 */
class Template
{
    use CurlHttp;

    /**
     * 设置所属行业
     *
     * @param string $accessToken
     * @param string $id1
     * @param string $id2
     *
     * @return \stlswm\WxSubscriptionPHP\Response
     * @Author wm
     * @Date   2018/12/29
     * @Time   14:42
     */
    public static function setIndustry(string $accessToken, string $id1, string $id2): Response
    {
        $url = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry?access_token={$accessToken}";
        return self::postJson($url, [
            'industry_id1' => $id1,
            'industry_id2' => $id2,
        ]);
    }


    /**
     * 获取设置的行业信息
     *
     * @param string $accessToken
     *
     * @return Response
     * @Author wm
     * @Date   2018/12/29
     * @Time   14:43
     */
    public static function getIndustry(string $accessToken)
    {
        $url = "https://api.weixin.qq.com/cgi-bin/template/get_industry?access_token={$accessToken}";
        return self::get($url);
    }

    /**
     * 获取模板列表
     *
     * @param string $accessToken
     *
     * @return Response
     * @Author wm
     * @Date   2018/8/18
     * @Time   15:27
     */
    public static function getAllPrivateTemplate(string $accessToken): Response
    {
        $url = "https://api.weixin.qq.com/cgi-bin/template/get_all_private_template?access_token={$accessToken}";
        return self::get($url);
    }
}