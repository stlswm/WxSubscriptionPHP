<?php

namespace stlswm\WxSubscriptionPHP\Message;

use stlswm\WxSubscriptionPHP\CurlHttp;
use stlswm\WxSubscriptionPHP\Response;

/**
 * Class TemplateMessage
 *
 * @package WxSubscriptionPHP\Message
 * @Date    2018/12/29
 * @Time    14:42
 */
class TemplateMessage
{
    use CurlHttp;

    private static $api = 'https://api.weixin.qq.com/cgi-bin/message/template/send';

    /**
     * @var string 接收者openid
     */
    public $toUser = '';
    /**
     * @var string 模板ID
     */
    public $templateId = '';
    /**
     * @var string url
     */
    public $url = '';
    /**
     * @var array 跳小程序所需数据，不需跳小程序可不用传该数据
     */
    public $miniProgram = [];
    /**
     * @var string 所需跳转到的小程序appid（该小程序appid必须与发模板消息的公众号是绑定关联关系，暂不支持小游戏）
     */
    public $appId = '';
    /**
     * @var string 所需跳转到小程序的具体页面路径，支持带参数,（示例index?foo=bar），暂不支持小游戏
     */
    public $pagePath = '';
    /**
     * @var array 模板数据
     */
    public $data = [];
    /**
     * @var string 模板内容字体颜色，不填默认为黑色
     */
    public $color = '';


    /**
     * 发送模板消息
     *
     * @param string $accessToken
     *
     * @return Response
     * @Author: wm
     * @Date  : 19-2-15
     * @Time  : 上午11:22
     */
    public function send(string $accessToken): Response
    {
        if (empty($accessToken)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数$accessToken不能为空';
            return $response;
        }
        $url = self::$api . "?access_token={$accessToken}";
        return self::postJson($url, [
            'touser'      => $this->toUser,
            'template_id' => $this->templateId,
            'url'         => $this->url,
            'miniprogram' => $this->miniProgram,
            'appid'       => $this->appId,
            'pagepath'    => $this->pagePath,
            'data'        => $this->data,
            'color'       => $this->color,
        ]);
    }
}