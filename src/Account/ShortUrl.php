<?php

namespace stlswm\WxSubscriptionPHP\Account;

use stlswm\WxSubscriptionPHP\CurlHttp;
use stlswm\WxSubscriptionPHP\Response;

/**
 * Class ShortUrl
 * 长链接转短链接接口
 * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1443433600
 *
 * @package WxSubscriptionPHP\Account
 * @Date    2018/12/29
 * @Time    14:01
 */
class ShortUrl
{
    use CurlHttp;

    private static $api = 'https://api.weixin.qq.com/cgi-bin/shorturl';

    /**
     * 长地址转短地址
     *
     * @param string $accessToken
     * @param string $longUrl
     *
     * @return Response
     * @Author wm
     * @Date   2018/12/29
     * @Time   14:03
     */
    public static function convert(string $accessToken, string $longUrl): Response
    {
        if (empty($accessToken)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数$accessToken不能为空';
            return $response;
        }
        if (empty($longUrl)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数$longUrl不能为空';
            return $response;
        }
        $api = self::$api . '?access_token=' . $accessToken;
        return self::postJson($api, [
            'access_token' => $accessToken,
            'action'       => 'long2short',
            'long_url'     => $longUrl,
        ]);
    }
}