<?php

namespace stlswm\WxSubscriptionPHP\Material;

use stlswm\WxSubscriptionPHP\CurlHttp;
use stlswm\WxSubscriptionPHP\Response;

/**
 * 素材管理
 */
class MaterialUtil
{
    use CurlHttp;


    /**
     * 获取素材总数
     * @param  string  $token
     * @return Response
     */
    public static function getMaterialCount(string $token): Response
    {
        return self::get("https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=$token");
    }


    /**
     * 获取素材列表
     * @param  string  $token
     * @param  string  $type
     * @param  int     $offset
     * @param  int     $count
     * @return Response
     */
    public static function batchGetMaterial(
        string $token,
        string $type = 'image',
        int $offset = 0,
        int $count = 10
    ): Response {
        return self::postJson("https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=$token",
            compact('type', 'offset', 'count'));
    }
}