<?php

namespace stlswm\WxSubscriptionPHP\ElectronicInvoice;

use stlswm\WxSubscriptionPHP\CurlHttp;
use stlswm\WxSubscriptionPHP\Response;

/**
 * Class InvoicePlatform
 * 开票平台接口列表
 * https://mp.weixin.qq.com/wiki?t=resource/res_main&id=21517918915Cvo2k
 *
 * @package WxSubscriptionPHP\ElectronicInvoice
 * @Date    2018/12/29
 * @Time    13:53
 */
class InvoicePlatform
{
    use CurlHttp;

    /**
     * 获取自身的开票平台识别码
     *
     * @param string $accessToken
     *
     * @return Response
     * @Author wm
     * @Date   2018/12/29
     * @Time   13:54
     */
    public static function SetUrl(string $accessToken): Response
    {
        if (empty($accessToken)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数$accessToken不能为空';
            return $response;
        }
        $api = "https://api.weixin.qq.com/card/invoice/seturl?access_token={$accessToken}";
        return self::postJson($api, new \StdClass());
    }
}