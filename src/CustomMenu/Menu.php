<?php

namespace stlswm\WxSubscriptionPHP\CustomMenu;

use stlswm\WxSubscriptionPHP\CurlHttp;
use stlswm\WxSubscriptionPHP\Response;

/**
 * Class Menu
 * @package WxSubscriptionPHP\CustomerMenu
 * @Date    2018/12/29
 * @Time    15:17
 */
class Menu
{
    use CurlHttp;

    /**
     * 将二级表中无限级分类的菜单转换成微信公众平台所有要分层的格式
     *
     * @param array $menus
     * @param int   $pid
     * @param array $keyMap 微信公众平台的字段与数据表字段的对应关系
     *
     * @return array
     * @Author wm
     * @Date   2018/12/29
     * @Time   15:21
     */
    public static function menuLayerFormat(
        array $menus,
        int $pid,
        $keyMap = [
            'id'       => 'id',
            'pid'      => 'pid',
            'type'     => 'type',
            'name'     => 'name',
            'key'      => 'key',
            'url'      => 'url',
            'media_id' => 'media_id',
            'appid'    => 'app_id',
            'pagepath' => 'page_path',
        ]
    ): array {
        $dataContainer = [];
        if (count($menus) == 0) {
            return $dataContainer;
        }
        foreach ($menus as $item) {
            if ($item[$keyMap['pid']] == $pid) {
                $subButton = self::menuLayerFormat($menus, $item[$keyMap['id']], $keyMap);
                if ($subButton) {
                    //如果有子菜单则只存在name与sub_button节点
                    $dataContainer[] = [
                        'name'       => $item[$keyMap['name']],
                        'sub_button' => $subButton,
                    ];
                } else {
                    $tmp = [];
                    foreach ($keyMap as $key => $dbColumn) {
                        $tmp[$key] = $item[$dbColumn];
                    }
                    $dataContainer[] = $tmp;
                }
            }
        }
        return $dataContainer;
    }

    /**
     * 设置自定义菜单（先将数据表中的数据调用menuLayerFormat组装成微信所要的格式）
     * @param  string  $accessToken
     * @param  array  $menu
     * @return Response
     * @Author wm
     * @Date   2018/12/29
     * @Time   15:18
     */
    public static function set(string $accessToken, array $menu): Response
    {
        if (empty($accessToken)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数$accessToken不能为空';
            return $response;
        }
        $url = 'https://api.weixin.qq.com/cgi-bin/menu/create?access_token='.$accessToken;
        return self::post($url, json_encode([
            'button' => $menu,
        ], JSON_UNESCAPED_UNICODE));
    }

    /**
     * 查询自定义菜单
     * @param  string  $accessToken
     * @return Response
     * @Author wm
     * @Date   2018/12/29
     * @Time   15:17
     */
    public static function search(string $accessToken): Response
    {
        if (empty($accessToken)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数$accessToken不能为空';
            return $response;
        }
        $url = 'https://api.weixin.qq.com/cgi-bin/menu/get?access_token='.$accessToken;
        return self::get($url);
    }

    /**
     * 删除自定义菜单
     * @param  string  $accessToken
     * @return Response
     * @Author wm
     * @Date   2018/12/29
     * @Time   15:17
     */
    public static function delete(string $accessToken): Response
    {
        if (empty($accessToken)) {
            $response = new Response();
            $response->result = false;
            $response->message = '参数$accessToken不能为空';
            return $response;
        }
        $url = 'https://api.weixin.qq.com/cgi-bin/menu/delete?access_token='.$accessToken;
        return self::get($url);
    }
}