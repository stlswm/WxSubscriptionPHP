<?php

namespace stlswm\WxSubscriptionPHP;

/**
 * Class Tools
 *
 * @package lib\weixin\subscription
 * @Date    2018/12/29
 * @Time    11:04
 */
class Tools
{
    /**
     * @param string $xml
     *
     * @return mixed
     * @Author wm
     * @Date   2018/12/29
     * @Time   11:06
     */
    public static function xmlToArray(string $xml)
    {
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $xml_string = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
        return json_decode(json_encode($xml_string), true);
    }
}