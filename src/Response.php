<?php

namespace stlswm\WxSubscriptionPHP;

class Response
{
    /**
     * @var bool $result
     */
    public $result = false;

    /**
     * @var string $message
     */
    public $message = '';

    /**
     * @var string $errCode
     */
    public $errCode = '';

    /**
     * @var string $errMsg
     */
    public $errMsg = '';

    /**
     * @var mixed 返回数据
     */
    public $data = [];
}