# 微信公众平台接口库

#### 介绍
WxSubscriptionPHP是一个php版本的微信公众平台接口库。
只完成了实际业务接口调用，不负责业务数据存储。

packagist地址 https://packagist.org/packages/stlswm/wx-subscription-php


#### 安装教程

composer require stlswm/wx-subscription-php

#### 使用说明

无

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request